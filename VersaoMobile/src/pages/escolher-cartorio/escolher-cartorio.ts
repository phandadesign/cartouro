import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireDatabase } from 'angularfire2/database'
import { Observable } from 'rxjs/Observable';
import { LoadingController } from 'ionic-angular';

import { TabsPage } from '../../pages/tabs/tabs';

/**
 * Generated class for the EscolherCartorioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-escolher-cartorio',
  templateUrl: 'escolher-cartorio.html',
})
export class EscolherCartorioPage {

  @ViewChild('cartorio') cartorio;

  Cartorios: Observable<any[]>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public db: AngularFireDatabase,
    public loadingCtrl: LoadingController
  ) {
    this.Cartorios = this.db.list("todasempresas/").snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EscolherCartorioPage');
  }

  OnInit() {

  }

  presentLoading(message: string) {
    let loader = this.loadingCtrl.create({
      content: message,
      duration: 3000
    });
    loader.present();
  }

  teste(cartorio: any) {
    console.log(cartorio);
    let CartorioSelecionadoDB = cartorio.caminho_db;
    localStorage.setItem("caminho_db", CartorioSelecionadoDB);
    this.presentLoading("Carregando");
    this.navCtrl.push(TabsPage);
  }

}

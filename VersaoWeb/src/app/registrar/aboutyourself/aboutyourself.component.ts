import { AuthService } from './../../_services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

import { first } from 'rxjs/internal/operators/first';

@Component({
  selector: 'aboutyourself',
  templateUrl: './aboutyourself.component.html',
  styleUrls: ['./aboutyourself.component.scss']
})
export class AboutyourselfComponent implements OnInit {
  public erro: any = {};
  public f: FormGroup;
  private codigo_empresa: string;
  public loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.f = this.formBuilder.group({
      nome: ['', Validators.required],
      telefone: ['', Validators.required],
      departamento: ['', Validators.required],
      cargo: ['', Validators.required]
    });
  }

  limpaErros() {
    this.erro = {};
  }

  next() {
    this.limpaErros();

    if (this.f.valid) {
      let empresa = this.afAuth.auth.currentUser;

      let data = {
        contas: {
          [`${empresa.uid}`]: {
            administrador: true,
            nome: this.f.value['nome'],
            telefone: this.f.value['telefone'],
            departamento: this.f.value['departamento'],
            cargo: this.f.value['cargo'],
            basededados: empresa.uid,
            email: empresa.email,
            status: 'Ocupado'
          }
        }
      };

      let info_empresa = JSON.parse(localStorage.getItem("info_empresa"));

      let params = { codigo_empresa: empresa.uid, data: data };

      this.authService.atualizarDadosEmpresa(params).then((result) => {

        this.authService.criarBaseDeDadosEmpresa(params).then((resultadoCriacao) => {

          this.authService.criarUsuarioGenerico(params).then((resultadoCriacaoUsuario) => {

            this.authService.cadastroGeralCartorio(info_empresa).then((cadastroGeralResult) => {

              this.loading = true;

              localStorage.setItem("usuario",JSON.stringify(this.afAuth.auth.currentUser));
              setTimeout(() => {
                localStorage.setItem("caminho_db", params.data.contas[`${empresa.uid}`].basededados);
                this.router.navigate(['agent/']);
              }, 4000);

            }).catch((e) => {
              console.log(e);
            });
            return;

          }).catch((error) => {
            console.log(error);
          });

        }).catch((error) => {
          console.log(error);
        });

      }).catch(erro => {
        console.log(erro);
      });
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GuichesComponent } from './guiches.component';

describe('GuichesComponent', () => {
  let component: GuichesComponent;
  let fixture: ComponentFixture<GuichesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuichesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GuichesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

const DEFAULT_URL = 'http://localhost:52900/';

const CONSTANTS = {
  'API': {
    'AUTH': `${DEFAULT_URL}/authentication/`,
    'USUARIOS': `${DEFAULT_URL}/usuarios/`
  }
}

export { CONSTANTS };
import { Component, OnInit, ViewChild } from '@angular/core';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { single } from './data';

import { AngularFireAuth } from 'angularfire2/auth';

import { AngularFireDatabase } from 'angularfire2/database'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  //Dados do grafico que aparece no dashbord
  single: any[];
  //Dados detalhados de um status especifico de protocolo
  details: any[];
  //Tamanho do grafico que aparece no dashboard
  //Width x Height
  view: any[] = [2000, 500];
  //Dados detalhados de um status especifico de protocolo
  //Width x Height
  view_detail: any[] = [780, 200];

  //Mostrar Legendas
  showLegend = true;

  // pie
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  //Esquema de Cores, só alterar o hexadecimal.
  //Esquema do gráfico principal
  colorScheme = {
    domain: ['#e53935', '#ffc400', '#5AA454']
  };

  //Esquema de Cores, só alterar o hexadecimal.
  //Esquema do gráfico detalhado
  colorSchemeDetail = {
    domain: ['#0277bd', '#039be5', '#00acc1', '#80d8ff']
  };

  public protocolos: Observable<any[]>;
  public CaminhoDB = localStorage.getItem("caminho_db");
  public today: string;
  public dataHoje: string;
  public filtro;
  //Variavel do modal
  public closeResult: string;
  //Variavel que referencia o modal para ser aberto
  @ViewChild('content') private content;
  public HiddenGraph: Boolean = false;
  public ShowGraph: Boolean = false;

  constructor(
    private afAuth: AngularFireAuth,
    public database: AngularFireDatabase,
    private modalService: NgbModal
  ) {


    let data = new Date();
    let ano = data.getFullYear().toString();
    let mes: any = data.getMonth() + 1;
    mes = mes.toString();

    if (mes.length == 1) {
      mes = "0" + mes;
    }
    var dia = data.getDate().toString();
    if (dia.length == 1) {
      dia = "0" + dia;
    }

    this.today = dia + "/" + mes + "/" + ano;
    this.dataHoje = ano + mes + dia;

    this.protocolos = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}`).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { key: snap.key }));
    }));

    this.protocolos.forEach(v => {

      let aberto = v.filter(value => value.status == "Aberto");
      let atendimento = v.filter(value => value.status == "Em Atendimento");
      let finalizado = v.filter(value => value.status == "Concluído");

      let value = [
        {
          "name": "Abertos",
          "value": aberto.length,
          "dados": aberto
        },
        {
          "name": "Em Atendimento",
          "value": atendimento.length,
          "dados": atendimento
        },
        {
          "name": "Fechados",
          "value": finalizado.length,
          "dados": finalizado
        }
      ];

      this.single = value;

      if (v.length == 0) {
        this.ShowGraph = false;
        this.HiddenGraph = true;
      } else {
        this.ShowGraph = true;
        this.HiddenGraph = false;
      }

    });

  }

  pushAbertos(value: any) {
    return new Promise(resolve => {
      resolve(value);
    });
  }

  ngOnInit() {
    console.log(this.afAuth.auth);
  }

  onSelect(event) {
    if (event.name == "Abertos") {
      this.filtro = "Aberto";
    } else if (event.name == "Em Atendimento") {
      this.filtro = "Em Atendimento";
    } else if (event.name == "Fechados") {
      this.filtro = "Concluído";
    }

    this.updateInfo();

    this.open(this.content);

  }

  updateInfo() {
    this.protocolos = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}`).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { key: snap.key }));
    }));

    this.protocolos.forEach(v => {

      let protocolosFiltrados = v.filter(value => value.status == this.filtro);
      let recFirma = protocolosFiltrados.filter(value => value.categoria == "Reconhecimento Firma");
      let procuracao = protocolosFiltrados.filter(value => value.categoria == "Procuracao");
      let registroCivil = protocolosFiltrados.filter(value => value.categoria == "Registro Civil");
      let escritura = protocolosFiltrados.filter(value => value.categoria == "Escritura");

      let value = [
        {
          "name": "Reconhecimento de Firma",
          "value": recFirma.length,
        },
        {
          "name": "Procuração",
          "value": procuracao.length,
        },
        {
          "name": "Registro Civil",
          "value": registroCivil.length,
        },
        {
          "name": "Escritura",
          "value": escritura.length,
        }
      ];

      this.details = value;

    });

  }

  open(content) {

    this.modalService.open(content, { size: 'lg' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
}

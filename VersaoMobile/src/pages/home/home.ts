import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database'
import { Observable } from 'rxjs/Observable';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  // Declaração de Variáveis
  //=================================
  CaminhoDB: string = localStorage.getItem("caminho_db");
  items: Observable<any[]>;
  itemsTotal: Observable<any[]>;
  itemsTotalPreferencial: Observable<any[]>;
  itemsTotalProcuracaoPreferencial: Observable<any[]>;
  itemsTotalRegistroCivilPreferencial: Observable<any[]>;
  ItemsTotalEscrituraPreferencial: Observable<any[]>;
  count: any = [];
  countPreferencial: any = [];
  countProcuracao: any = [];
  countProcuracaoPreferencial: any = [];
  countRegistroCivil: any = [];
  countRegistroCivilPreferencial: any = [];
  countEscritura: any = [];
  countEscrituraPreferencial: any = [];
  validationProcuracao: any;
  validationProcuracaoPreferencial: any;
  validation: any;
  validationPreferencial: any;
  validationRegistroCivil: any;
  validationRegistroCivilPreferencial: any;
  validationEscritura: any;
  validationEscrituraPreferencial: any;
  today: any;
  todayGeracao: any;
  ticketReconhecimentoFirma: string;
  ticketProcuracao: string;
  ticketRegistroCivil: string;
  ticketEscritura: string;
  public ticketAtualRecochecimentoFirma: Observable<any[]>;
  public ticketAtualProcuracao: Observable<any[]>;
  public ticketAtualRegistroCivil: Observable<any[]>;
  public ticketAtualEscritura: Observable<any[]>;
  ticketEspecifico: Observable<any[]>;
  itemsTotalProcuracao: Observable<any[]>;
  itemsTotalRegistroCivil: Observable<any[]>;
  itemsTotalEscritura: Observable<any[]>;
  validaStatusReconhecimentoFirma: string = 'Concluído';
  validaStatusRegistroCivil: string = 'Concluído';
  validaStatusProcuracao: string = 'Concluído';
  validaStatusEscritura: string = 'Concluído';
  validaPreferencial: boolean = false;
  //=================================
  todosTickets: Observable<any[]>;
  constructor(public navCtrl: NavController, public database: AngularFireDatabase) {
    this.getToday();
    let dataHoje = this.todayGeracao;

    //Captura dados do firebase
    this.todosTickets = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    });

    //RECONHECIMENTO FIRMA NORMAL
    this.todosTickets.forEach(item => {
       //Reconhecimento Firma
       this.count = [];
       this.count.push(item.filter(i => i.categoria == "Reconhecimento Firma" && i.typeTicket == "Normal" && i.status == "Aberto"));
       this.validation = this.count[0].length;
       //Reconhecimento Firma Preferencial
       this.countPreferencial = [];
       this.countPreferencial.push(item.filter(i => i.categoria == "Reconhecimento Firma" && i.typeTicket == "Preferencial" && i.status == "Aberto"));
       this.validationPreferencial = this.countPreferencial[0].length;
       console.log(this.countPreferencial);
       //Procuracao NORMAL
       this.countProcuracao = [];
       this.countProcuracao.push(item.filter(i => i.categoria == "Procuracao" && i.typeTicket == "Normal" && i.status == "Aberto"));
       this.validationProcuracao = this.countProcuracao[0].length;
       console.log(this.countProcuracao);
       //Procuracao Preferencial
       this.countProcuracaoPreferencial = [];
       this.countProcuracaoPreferencial.push(item.filter(i => i.categoria == "Procuracao" && i.typeTicket == "Preferencial" && i.status == "Aberto"));
       this.validationProcuracaoPreferencial = this.countProcuracaoPreferencial[0].length;
       console.log(this.countProcuracaoPreferencial);
       //Registro Civil NORMAL
       this.countRegistroCivil = [];
       this.countRegistroCivil.push(item.filter(i => i.categoria == "Registro Civil" && i.typeTicket == "Normal" && i.status == "Aberto"));
       this.validationRegistroCivil = this.countRegistroCivil[0].length;
       console.log(this.countRegistroCivil);
       //Registro Civil Preferencial
       this.countRegistroCivilPreferencial = [];
       this.countRegistroCivilPreferencial.push(item.filter(i => i.categoria == "Registro Civil" && i.typeTicket == "Preferencial" && i.status == "Aberto"));
       this.validationRegistroCivilPreferencial = this.countRegistroCivilPreferencial[0].length;
       console.log(this.countRegistroCivilPreferencial);
       //Escritura NORMAL
       this.countEscritura = [];
       this.countEscritura.push(item.filter(i => i.categoria == "Escritura" && i.typeTicket == "Normal" && i.status == "Aberto"));
       this.validationEscritura = this.countEscritura[0].length;
       console.log(this.countEscritura);
       //Escritura Preferencial
       this.countEscrituraPreferencial = [];
       this.countEscrituraPreferencial.push(item.filter(i => i.categoria == "Escritura" && i.typeTicket == "Preferencial" && i.status == "Aberto"));
       this.validationEscrituraPreferencial = this.countEscrituraPreferencial[0].length;
       console.log(this.countEscrituraPreferencial);
    });
    //==================================

  }

  getToday() {

    let data = new Date();
    let ano = data.getFullYear().toString();
    let mes:any = data.getMonth() + 1;
    mes = mes.toString();

    if(mes.length == 1){
        mes = "0" + mes;
    }
    var dia = data.getDate().toString();
    if(dia.length == 1){
        dia = "0" + dia;
    }
    this.todayGeracao = ano + mes + dia;
    return this.today = dia + "/" + mes + "/" + ano;
  }

  reconhecimentoFirmaAutenticacao() {
    let tamanho = this.count[0].length -1;
    //Preferencial
    let tamanhoPreferencial = this.countPreferencial[0].length -1;

    let dataHoje = this.todayGeracao;

    if (this.validaPreferencial === false) {
      if (this.validation === 0) {
        //`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}` + '/reconhecimentoFirma'
        this.ticketReconhecimentoFirma = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'B1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Normal', categoria: 'Reconhecimento Firma' }).key;
      } else {

        this.ticketReconhecimentoFirma = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'B' + (this.count[0][tamanho].number  + 1),
            status: 'Aberto',
            number: this.count[0][tamanho].number  + 1,
            date: this.today,
            typeTicket: 'Normal',
            categoria: 'Reconhecimento Firma'
          }).key;

      }
    } else if (this.validaPreferencial === true) {
      if (this.validationPreferencial === 0) {
        this.ticketReconhecimentoFirma = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'BP1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Preferencial', categoria: 'Reconhecimento Firma' }).key;
      } else {

        this.ticketReconhecimentoFirma = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'BP' + (this.countPreferencial[0][tamanhoPreferencial].number + 1),
            status: 'Aberto',
            number: this.countPreferencial[0][tamanhoPreferencial].number + 1,
            date: this.today,
            typeTicket: 'Preferencial',
            categoria: 'Reconhecimento Firma'
          }).key;

      }
    }
    this.ticketAtualRecochecimentoFirma = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`, ref => ref.orderByKey().equalTo(this.ticketReconhecimentoFirma)).snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    });

    this.ticketAtualRecochecimentoFirma.forEach(item => {
      this.validaStatusReconhecimentoFirma = item[0].status;
      console.log(this.validaStatusReconhecimentoFirma);
    });

    console.log(this.ticketReconhecimentoFirma);
    console.log(this.ticketAtualRecochecimentoFirma);
  }

  procuracao() {
    //Index 0 do array contem todo o conteudo do array, o tamanho dele -1 será igual ao número de senhas daquele tipo para pesquisa no array
    let tamanho = this.countProcuracao[0].length - 1
    //Index 0 do array contem todo o conteudo do array, o tamanho dele -1 será igual ao número de senhas daquele tipo para pesquisa no array
    let tamanhoPreferencial = this.countProcuracaoPreferencial[0].length -1;

    let dataHoje = this.todayGeracao;


    if (this.validaPreferencial === false) {

      if (this.validationProcuracao === 0) {
        this.ticketProcuracao = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'P1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Normal', categoria: 'Procuracao' }).key;
      } else {

        this.ticketProcuracao = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'P' + (this.countProcuracao[0][tamanho].number + 1),
            status: 'Aberto',
            number: this.countProcuracao[0][tamanho].number + 1,
            date: this.today,
            typeTicket: 'Normal',
            categoria: 'Procuracao'
          }).key;

      }
    } else if (this.validaPreferencial === true) {
      if (this.validationProcuracaoPreferencial === 0) {
        this.ticketProcuracao = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'PP1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Preferencial', categoria: 'Procuracao' }).key;
      } else {

        this.ticketProcuracao = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'PP' + (this.countProcuracaoPreferencial[0][tamanhoPreferencial].number + 1),
            status: 'Aberto',
            number: this.countProcuracaoPreferencial[0][tamanhoPreferencial].number + 1,
            date: this.today,
            typeTicket: 'Preferencial',
            categoria: 'Procuracao'
          }).key;

      }
    }
    this.ticketAtualProcuracao = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`, ref => ref.orderByKey().equalTo(this.ticketProcuracao)).snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    });
    this.ticketAtualProcuracao.forEach(item => {
      console.log(item);
      this.validaStatusProcuracao = item[0].status;
      console.log(this.validaStatusProcuracao);
    });

    console.log(this.ticketProcuracao);
    console.log(this.ticketAtualProcuracao);
  }



  registroCivil() {
    //Index 0 do array contem todo o conteudo do array, o tamanho dele -1 será igual ao número de senhas daquele tipo para pesquisa no array
    let tamanho = this.countRegistroCivil[0].length - 1
    //Index 0 do array contem todo o conteudo do array, o tamanho dele -1 será igual ao número de senhas daquele tipo para pesquisa no array
    let tamanhoPreferencial = this.countRegistroCivilPreferencial[0].length - 1

    let dataHoje = this.todayGeracao;


    if (this.validaPreferencial === false) {
      if (this.validationRegistroCivil === 0) {
        this.ticketRegistroCivil = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'C1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Normal', categoria: 'Registro Civil' }).key;
      } else {

        this.ticketRegistroCivil = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'C' + (this.countRegistroCivil[0][tamanho].number + 1),
            status: 'Aberto',
            number: this.countRegistroCivil[0][tamanho].number + 1,
            date: this.today,
            typeTicket: 'Normal',
             categoria: 'Registro Civil'
          }).key;

      }
    } else if (this.validaPreferencial === true) {
      if (this.validationRegistroCivilPreferencial === 0) {
        this.ticketRegistroCivil = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'CP1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Preferencial',  categoria: 'Registro Civil' }).key;
      } else {

        this.ticketRegistroCivil = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'CP' + (this.countRegistroCivilPreferencial[0][tamanhoPreferencial].number + 1),
            status: 'Aberto',
            number: this.countRegistroCivilPreferencial[0][tamanhoPreferencial].number + 1,
            date: this.today,
            typeTicket: 'Preferencial',
             categoria: 'Registro Civil'
          }).key;

      }
    }
    this.ticketAtualRegistroCivil = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`, ref => ref.orderByKey().equalTo(this.ticketRegistroCivil)).snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    });
    this.ticketAtualRegistroCivil.forEach(item => {
      this.validaStatusRegistroCivil = item[0].status;
      console.log(this.validaStatusRegistroCivil);
    });

    console.log(this.ticketRegistroCivil);
    console.log(this.ticketAtualRegistroCivil);
  }

  escritura() {
    //Index 0 do array contem todo o conteudo do array, o tamanho dele -1 será igual ao número de senhas daquele tipo para pesquisa no array
    let tamanho = this.countEscritura[0].length - 1;
    //preferencial
    let tamanhoPreferencial = this.countEscrituraPreferencial[0].length - 1

    let dataHoje = this.todayGeracao;

    if (this.validaPreferencial === false) {

      if (this.validationEscritura === 0) {
        this.ticketEscritura = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'E1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Normal',  categoria: 'Escritura' }).key;
      } else {

        this.ticketEscritura = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'E' + (this.countEscritura[0][tamanho].number + 1),
            status: 'Aberto',
            number: this.countEscritura[0][tamanho].number + 1,
            date: this.today,
            typeTicket: 'Normal',
             categoria: 'Escritura'
          }).key;

      }
    } else if (this.validaPreferencial === true) {

      if (this.validationEscrituraPreferencial === 0) {
        this.ticketEscritura = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push({ ticket: 'EP1', status: 'Aberto', number: 1, date: this.today, typeTicket: 'Preferencial', categoria: 'Escritura' }).key;
      } else {

        this.ticketEscritura = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`).push(
          {

            ticket: 'EP' + (this.countEscrituraPreferencial[0][tamanhoPreferencial].number + 1),
            status: 'Aberto',
            number: this.countEscrituraPreferencial[0][tamanhoPreferencial].number + 1,
            date: this.today,
            typeTicket: 'Preferencial',
            categoria: 'Escritura'
          }).key;

      }

    }
    this.ticketAtualEscritura = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${dataHoje}`, ref => ref.orderByKey().equalTo(this.ticketEscritura)).snapshotChanges().map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { $key: snap.key }))
    });
    this.ticketAtualEscritura.forEach(item => {
      this.validaStatusEscritura = item[0].status;
      console.log(this.validaStatusEscritura);
    });

    console.log(this.ticketEscritura);
    console.log(this.ticketAtualEscritura);
  }

}

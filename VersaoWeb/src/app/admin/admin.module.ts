import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { FuncionariosComponent } from './funcionarios/funcionarios.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';
import { GuichesComponent } from './guiches/guiches.component';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule
  ],
  declarations: [FuncionariosComponent, RelatoriosComponent, GuichesComponent, ConfiguracoesComponent, DashboardComponent]
})
export class AdminModule { }

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './../../_services/auth.service';

@Component({
  selector: 'ctrnavbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  options = new Array();
  private suboptions = new Array();
  private url: any;

  constructor(
    private router: Router,
    private authService: AuthService
  ) {
    this.url = this.router.routerState.snapshot.url;
  }

  ngOnInit() {
    this.setMenu();
  }

  //Base tickets
  // https://fontawesome.com/icons?d=gallery
  setMenu() {
    this.options = [
      { title: 'Dashboard', href: '/agent/dashboard', class: 'fa fa-home' },
      { title: 'Atendimentos', href: '/agent/atendimentos', class: 'fa fa-ticket-alt' },
      { title: 'Visualizações', href: '/agent/visualizacoes', class: 'fa fa-stream' },
      // { title: 'Relatórios', href: '/agent/relatorios', class: 'fa fa-chart-bar' },
      // { title: 'Gerenciamento de Usuarios', href: 'usuarios', class: 'fa fa-user-alt'},
      { title: 'Gerenciamento de Usuarios', href: '/agent/criar-usuario', class: 'fa fa-user-plus'},
      { title: 'Configurações', href: '/agent/configuracoes', class: 'fa fa-cog' }
    ];
    this.suboptions = [
      { title: 'Atender Senhas', href: 'atender-senha', class: 'fa fa-home' },
      { title: 'Criar Usuários', href: 'criar-usuario', class: 'fa fa-user' }
    ]
  }

}

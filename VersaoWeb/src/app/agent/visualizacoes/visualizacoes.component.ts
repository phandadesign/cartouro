import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';

import {AngularFireDatabase} from 'angularfire2/database'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'visualizacoes',
  templateUrl: './visualizacoes.component.html',
  styleUrls: ['./visualizacoes.component.scss']
})
export class VisualizacoesComponent implements OnInit {

  TICKETS_ABERTOS: Array<any>;
  displayedColumns = ['ticket', 'categoria', 'date', 'status'];
  dataSource = new MatTableDataSource<Ticket>(this.TICKETS_ABERTOS);

  public protocolos: Observable<any[]>;
  public CaminhoDB = localStorage.getItem("caminho_db");
  public today: string;
  public dataHoje: string;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

  constructor(public database: AngularFireDatabase){

    let data = new Date();
    let ano = data.getFullYear().toString();
    let mes:any = data.getMonth() + 1;
    mes = mes.toString();

    if(mes.length == 1){
        mes = "0" + mes;
    }
    var dia = data.getDate().toString();
    if(dia.length == 1){
        dia = "0" + dia;
    }

    this.today = dia + "/" + mes + "/" + ano;
    this.dataHoje = ano + mes + dia;

    this.protocolos = this.database.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}`, ref => ref.orderByChild('date').equalTo(this.today)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { key: snap.key }) );
    }));

    this.protocolos.forEach(item => {
      this.TICKETS_ABERTOS = [];
      let ticketFiltrado = item.filter(i => i.status == "Aberto");
      ticketFiltrado.forEach(i => {
          this.TICKETS_ABERTOS.push({
            ticket: i.ticket,
            categoria: i.categoria,
            date: i.date,
            status: i.status
          });
      });

      this.dataSource = new MatTableDataSource<Ticket>(this.TICKETS_ABERTOS);
    });



  }
}


export interface Ticket {
  ticket: string;
  categoria: string;
  data: string;
  status: string;
}

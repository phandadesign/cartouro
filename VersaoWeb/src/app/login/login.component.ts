import { Component, OnInit } from '@angular/core';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../_services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loading = false;
  erro: any = {};
  f: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    if (!window.localStorage.getItem('remember')) {
      this.authService.deslogaUsuario();
    } else {
      this.loading = true;
      setTimeout(() => {
        this.router.navigate(['agent/']);
      }, 4000);
    }

    this.f = this.formBuilder.group({
      email: ['', Validators.required],
      senha: ['', Validators.required],
      remember: [false]
    });
  }

  limpaErros() {
    this.erro = {};
  }

  doLogin() {
    this.erro = {};
    let erroLogin = '';

    this.limpaErros();

    if (this.f.controls.email.value == '') {
      this.erro.email = 'Informe um email correto';
    }

    if (this.f.controls.senha.value < 6) {
      this.erro.senha = 'Informe uma senha correta';
    }

    if (this.f.controls.remember.value == true) {
      window.localStorage.setItem('remember', '1');
    }

    if (this.f.valid) {
      this.loading = true;
      this.authService.login(this.f.controls.email.value, this.f.controls.senha.value)
        .then(() => {
          //Pega o ramo da base de dados do usuario no banco
          this.authService.pegarBaseDeDados(this.f.controls.email.value);
          localStorage.setItem("Guiche", "");
          setTimeout(() => {
            this.loading = false;
            this.router.navigate(['/agent/dashboard']);
          }, 3000);
        })
        .catch((error) => {
          var errorCode = error.code;
          var errorMessage = error.message;

          if(errorMessage == "auth/invalid-email"){
            erroLogin = "Email ou senha Incorretos";
          }else if(errorMessage == "auth/wrong-password"){
            erroLogin = "Email ou senha Incorretos";
          }else if(errorMessage == "auth/user-not-found"){
            erroLogin = "Usuário Não Cadastrado";
          }else if(errorMessage == "The email address is badly formatted."){
            erroLogin = "Insira um Email válido";
          }else{
            erroLogin = "Usuário ou senha Inválido";
          }
          setTimeout(() => {
            this.loading = false;
            if(erroLogin != ''){
              alert(erroLogin);
            }
          }, 3000);
        });
    }
  }

}

import { NgModule } from '@angular/core';

import { ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { environment } from '../../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { LoadingModule } from 'ngx-loading';

@NgModule({
  imports: [
    AngularMaterialModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    LoadingModule
  ],
  exports: [
    ReactiveFormsModule,
    AngularMaterialModule,
    LoadingModule
  ],
  declarations: []
})
export class SharedModule { }

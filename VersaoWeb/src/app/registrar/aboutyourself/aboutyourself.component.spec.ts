import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutyourselfComponent } from './aboutyourself.component';

describe('AboutyourselfComponent', () => {
  let component: AboutyourselfComponent;
  let fixture: ComponentFixture<AboutyourselfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutyourselfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutyourselfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

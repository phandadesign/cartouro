import { AboutyourselfComponent } from './aboutyourself/aboutyourself.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegistrarComponent } from './registrar.component';
import { CompanyComponent } from './company/company.component';
import { GetstartedComponent } from './getstarted/getstarted.component';

const routes: Routes = [
  { path: '', redirectTo: 'getstarted', pathMatch: 'full' },
  { path: 'getstarted', component: GetstartedComponent },
  { path: 'companhia', component: CompanyComponent },
  { path: 'sobrevoce', component: AboutyourselfComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrarRoutingModule { }

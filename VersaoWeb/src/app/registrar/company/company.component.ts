import { AngularFireAuth } from 'angularfire2/auth';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { AuthService } from '../../_services/auth.service';

@Component({
  selector: 'company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {
  public erro: any = {};
  public f: FormGroup;
  ndf = ["1-9", "10-299", "300-599", "600-999", "1000-4999", "+5000"];

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.f = this.formBuilder.group({
      empresa: ['', Validators.required],
      numeroDeFuncionarios: ['', Validators.required]
    });
  }

  limpaErros() {
    this.erro = {};
  }

  next() {
    this.limpaErros();

    if (this.f.valid) {
      let empresa = this.afAuth.auth.currentUser;

      empresa.updateProfile({
        displayName: this.f.value['empresa'],
        photoURL: 'https://cartouro.io/assets/profile.png'
      });

      let data = {
        nome_empresa: this.f.value['empresa'],
        photoURL: empresa.photoURL,
        numero_de_funcionarios: this.f.value['numeroDeFuncionarios']
      };

      localStorage.setItem("info_empresa", JSON.stringify({
        "nome_empresa": this.f.value['empresa'],
        "caminho_db": empresa.uid
      }));

      let params = { codigo_empresa: empresa.uid, data: data };

      this.authService.atualizarDadosEmpresa(params).then((result) => {
        this.router.navigate(['registrar/sobrevoce']);
      }).catch((erro) => { console.log(erro) });
    }
  }

}

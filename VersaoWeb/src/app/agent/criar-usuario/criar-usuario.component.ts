import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from './../../_services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'criar-usuario',
  templateUrl: './criar-usuario.component.html',
  styleUrls: ['./criar-usuario.component.scss']
})
export class CriarUsuarioComponent implements OnInit {
  erro: any = {};
  f: FormGroup;
  Administrador: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private afAuth: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.f = this.formBuilder.group({
      nome: ['', Validators.required],
      email: ['', Validators.required],
      senha: ['', Validators.required],
      cargo: ['', Validators.required],
      departamento: ['', Validators.required],
      administrador: ''
    });



  }

  limpaErros() {
    this.erro = {};
  }

  CriarUsuario(){
    this.erro = {};

    this.limpaErros();

    if (this.f.controls.email.value == '') {
      this.erro.email = 'Informe um email correto';
    }

    if(this.f.controls.nome.value == ''){
      this.erro.nome = 'Informe o nome do Usuário';
    }

    if (this.f.controls.senha.value < 6) {
      this.erro.senha = 'A Senha deve conter no minimo 6 caracteres';
    }

    if (this.f.controls.cargo.value == '') {
      this.erro.cargo = 'Informe o cargo do Usuário';
    }

    if (this.f.controls.cargo.value == '') {
      this.erro.departamento = 'Informe o departamento do Usuário';
    }

    if (this.f.valid){

      if(this.f.controls.administrador.value == false || this.f.controls.administrador.value == ""){
        this.Administrador = false;
      } else {
        this.Administrador = true;
      }

      let empresaID = JSON.parse(localStorage.getItem("usuario"));

      let data = {
        administrador: this.Administrador,
        nome: this.f.value['nome'],
        email: this.f.value['email'],
        departamento: this.f.value['departamento'],
        cargo: this.f.value['cargo'],
        basededados: localStorage.getItem("caminho_db"),
        status: 'Ocupado'
      };

      this.authService.registrarUsuario(this.f.controls.email.value, this.f.controls.senha.value).then((r) => {

        this.authService.adicionarUsuarioAoCartorio(data, data.basededados, r.user.uid).then((resultado) => {
            
            this.authService.criarUsuarioPelaEmpresa(data, r.user.uid).then((result) => {

            alert("Usuário Cadastrado com Sucesso");


            setTimeout(() => {

              this.router.navigate(['/agent/dashboard']);
            }, 3000);

          }).catch((error) => {
            this.erro = error;
          });

        }).catch((error) => {
          console.log(error);
        });



        })
        .catch((erro) => {
          setTimeout(() => {
            this.erro = erro;
          }, 3000);
        });
    }

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtenderSenhaComponent } from './atender-senha.component';

describe('AtenderSenhaComponent', () => {
  let component: AtenderSenhaComponent;
  let fixture: ComponentFixture<AtenderSenhaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtenderSenhaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtenderSenhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

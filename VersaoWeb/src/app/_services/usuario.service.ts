import { Injectable } from '@angular/core';
import { Usuario } from '../_models/usuario';
import { Empresa } from '../_models/empresa';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private usuarios: Usuario;
  private empresa: Empresa;

  constructor(private afDatabase: AngularFireDatabase) { }

  public getUsuarios(empresa: Empresa){
    return this.afDatabase.object(`/empresas/${empresa.key}/contas/`).valueChanges();
  }

  public atualizarUsuario(usuario: any, uid: string){
    return this.afDatabase.object(`/usuarios/${uid}/`).set(usuario);
  }

  public atualizarUsuarioDaEmpresa(base: string, usuario: any, uid: string){
    return this.afDatabase.object(`/empresas/${base}/contas/${uid}`).set(usuario);
  }


}

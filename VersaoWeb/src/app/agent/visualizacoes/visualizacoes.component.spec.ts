import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizacoesComponent } from './visualizacoes.component';

describe('VisualizacoesComponent', () => {
  let component: VisualizacoesComponent;
  let fixture: ComponentFixture<VisualizacoesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizacoesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizacoesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

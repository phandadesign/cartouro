import { AuthService } from './../../_services/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { Empresa } from '../../_models/empresa';
import { map } from 'rxjs/operators';

@Component({
  selector: 'usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.scss']
})
export class UsuariosComponent implements OnInit {
  
  empresa: Empresa;
  displayedColumns = ['nome', 'email', 'criado_em', 'conectado', 'uid'];
  dataSource = new MatTableDataSource<PeriodicElement>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private authService: AuthService){
    this.authService.getCurrentEmpresa().pipe(
      map(data => console.log(data))
    );
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}

export interface PeriodicElement {
  nome: string;
  email: string;
  criado_em: string;
  conectado: boolean;
  uid: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' },
  { nome: 'Bruno Leone Alexandre Pacheco Corrêa', email: 'leonecbruno@gmail.com', criado_em: '05/06/2018', conectado: true, uid: '23342kjf23flksdfasd' }
];
import { Component, OnInit } from '@angular/core';

import {AngularFireDatabase} from 'angularfire2/database'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/internal/operators/first';


@Component({
  selector: 'atender-senha',
  templateUrl: './atender-senha.component.html',
  styleUrls: ['./atender-senha.component.scss']
})
export class AtenderSenhaComponent implements OnInit {

    //Reconhecimento de Firma
    public protocolosAtendimentoRDF: Observable<any[]>;
    //Escritura
    public protocolosAtendimentoESC: Observable<any[]>;
    //Registro Civil
    public protocolosAtendimentoRGC: Observable<any[]>;
    //Procuração
    public protocolosAtendimentoPRC: Observable<any[]>;

    public AtendimentoAtual: any;
    public EmAtendimento: boolean;
    public CaixaLivre: boolean = true;
    public tipoSenha: string;
    public Preferencial: boolean;

    public CaminhoDB = localStorage.getItem("caminho_db");
    public Guiche = localStorage.getItem("Guiche");

    public Numero_Guiche: any;

    public today: string;
    public dataHoje: string;
    public checked;

    public tickets: Array<any>;
    public _tickets: Array<any>;

    public todosProtocolos: Observable<any[]>;
    public ticketsTodosStatus: Observable<any[]>;

    f: FormGroup;

    tipo_senhas = [
      {value: 'RDF', viewValue: 'Reconhecimento de Firma'},
      {value: 'ESC', viewValue: 'Escritura'},
      {value: 'RGC', viewValue: 'Registro Civil'},
      {value: 'PRC', viewValue: 'Procuração'}
    ];

  constructor(
    public db: AngularFireDatabase,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.f = this.formBuilder.group({
      Preferencial: '',
      tipoSenha: ['', Validators.required]
    });

    let data = new Date();
    let ano = data.getFullYear().toString();
    let mes:any = data.getMonth() + 1;
    mes = mes.toString();

    if(mes.length == 1){
        mes = "0" + mes;
    }
    var dia = data.getDate().toString();
    if(dia.length == 1){
        dia = "0" + dia;
    }

    this.today = dia + "/" + mes + "/" + ano;
    this.dataHoje = ano + mes + dia;

    console.log(this.today);

    this.todosProtocolos =  this.db.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}`, ref => ref.orderByChild('status').equalTo('Aberto') && ref.orderByChild('date').equalTo(this.today)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { key: snap.key }) );
    }));

    this.ticketsTodosStatus = this.db.list(`bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}`, ref => ref.orderByChild('date').equalTo(this.today)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { key: snap.key }) );
    }));

    this.ticketsTodosStatus.forEach(item => {
      this.tickets = [];
      this.tickets.push(item);
    })

  }

  AtenderSenha() {

    if(this.f.controls.Preferencial.value == false || this.f.controls.Preferencial.value == ""){
      this.Preferencial = false;
    } else {
      this .Preferencial = true;
    }

    this.tipoSenha = this.f.controls.tipoSenha.value;

    let senhaAtendida: any;
    let idSenhaAtendida: any;
    let itens: Observable<any>;
    let caminhoDB;
    let tipoTicket;
    let categoriaTicket

    if(this.tipoSenha != ""){
      if(this.Preferencial == true){
        tipoTicket = "Preferencial";
      } else {
        tipoTicket = "Normal";
      }

      caminhoDB = `bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}/`;
      itens = this.todosProtocolos;

      switch(this.tipoSenha){
        case "RDF":
            categoriaTicket = "Reconhecimento Firma";
          break;

        case "ESC":
            categoriaTicket = "Escritura";
          break;

        case "RGC":
            categoriaTicket = "Registro Civil";
          break;

        case "PRC":
            categoriaTicket = "Procuracao";
          break;
      }

      let itemFiltrado;

      itens.pipe(first()).forEach(item => {
        itemFiltrado = item.filter(i => i.typeTicket === tipoTicket && i.status === "Aberto" && i.categoria === categoriaTicket);

        if(itemFiltrado[0] != null && itemFiltrado[0].typeTicket == tipoTicket){
          senhaAtendida = itemFiltrado[0];
          idSenhaAtendida = itemFiltrado[0].key;
          senhaAtendida.status = "Em Atendimento";
          console.log(this.db.object(caminhoDB + idSenhaAtendida));
          this.db.object(caminhoDB + idSenhaAtendida).update(senhaAtendida);
          this.db.object(caminhoDB + idSenhaAtendida).update({"guiche": this.Guiche});
          this.CaixaLivre = false;
          this.EmAtendimento = true;
          this.AtendimentoAtual = senhaAtendida;
          console.log("aqui")
        } else {
          alert("Nao há senhas deste tipo em aberto para atendimento");
          return "Nao há senhas deste tipo em aberto para atendimento";
        }
      })


    } else {
      alert("Escolha o tipo de Senha");
    }

  }

  FinalizarAtendimento(){
    let senhaAtendida: any;
    let idSenhaAtendida: any;
    let itens: Observable<any>;
    let caminhoDB;
    let tipoTicket;
    let categoriaTicket;

    caminhoDB = `bancodedadosgeral/${this.CaminhoDB}/tickets/${this.dataHoje}/`;

    switch(this.tipoSenha){
      case "RDF":
          categoriaTicket = "Reconhecimento Firma";
        break;

      case "ESC":
          categoriaTicket = "Escritura";
        break;

      case "RGC":
          categoriaTicket = "Registro Civil";
        break;

      case "PRC":
          categoriaTicket = "Procuracao";
        break;
    }

    idSenhaAtendida = this.AtendimentoAtual.key;
    this.AtendimentoAtual.status = "Concluído";
    console.log(this.AtendimentoAtual);
    this.db.object(caminhoDB + idSenhaAtendida).update(this.AtendimentoAtual);
    alert("Atendimento ticket " + this.AtendimentoAtual.ticket + " Finalizado!");
    this.CaixaLivre = true;
    this.EmAtendimento = false;
  }

  //Valida a entrada de texto no input para apenas numeros
  validarEntradaGuiche(e){
    let exp = /^\d$/;
    let expSetas = /^Arrow.*$/;

    if(exp.test(e.key) || e.key == "Backspace" || expSetas.test(e.key) || e.key == "Home" || e.key == "End"){
      console.log(e.key);
    } else {
      e.preventDefault();
    }
  }

  ConfirmarGuiche(){
    if(this.Numero_Guiche == "" || this.Numero_Guiche == undefined){
      alert("Digite o número do Guiche");
    } else {
      this.Guiche = this.Numero_Guiche;
      localStorage.setItem("Guiche", this.Numero_Guiche);
    }

  }

}

import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioService } from './../../_services/usuario.service';


@Component({
  selector: 'configuracoes',
  templateUrl: './configuracoes.component.html',
  styleUrls: ['./configuracoes.component.scss']
})
export class ConfiguracoesComponent implements OnInit {

  uid = JSON.parse(localStorage.getItem('current_user')).uid;
  info_user = JSON.parse(localStorage.getItem('info_user'));
  caminho_db = localStorage.getItem("caminho_db");

  erro: any = {};
  f: FormGroup;

  constructor(public formBuilder: FormBuilder, public userService: UsuarioService) { }

  ngOnInit() {
    this.f = this.formBuilder.group({
      nome: ['', Validators.required],
      cargo: ['', Validators.required],
      departamento: ['', Validators.required],
      telefone: ['', Validators.required]
    });

    this.f.controls.nome.setValue(this.info_user.nome);
    this.f.controls.cargo.setValue(this.info_user.cargo);
    this.f.controls.departamento.setValue(this.info_user.departamento);
    this.f.controls.telefone.setValue(this.info_user.telefone);

  }

    ConfirmarAlteracoes(){

    this.erro = {};

    this.limpaErros();


    if(this.f.controls.nome.value == ''){
      this.erro.nome = 'Informe o nome do Usuário';
    }

    if (this.f.controls.cargo.value == '') {
      this.erro.cargo = 'Informe o cargo do Usuário';
    }

    if (this.f.controls.departamento.value == '') {
      this.erro.departamento = 'Informe o departamento do Usuário';
    }

    if (this.f.controls.telefone.value == '') {
      this.erro.telefone = 'Informe o telefone do Usuário';
    }

    if (this.f.valid){
      this.info_user.nome = this.f.controls.nome.value;
      this.info_user.cargo = this.f.controls.cargo.value;
      this.info_user.departamento = this.f.controls.departamento.value;
      this.info_user.telefone = this.f.controls.telefone.value;

      this.userService.atualizarUsuario(this.info_user, this.uid).then(result => {
        this.userService.atualizarUsuarioDaEmpresa(this.caminho_db, this.info_user, this.uid).then(r => {
            alert('Usuário atualizado com sucesso!');
            localStorage.setItem('info_user', JSON.stringify(this.info_user));
        }).catch(e => {
          alert(e);
        });
      }).catch(e => {
        alert(e);
      });

    }

  }

  limpaErros() {
    this.erro = {};
  }

  validarTelefone(e){
    let exp = /^\d$/;
    let expSetas = /^Arrow.*$/;

    if(exp.test(e.key) || e.key == "Backspace" || expSetas.test(e.key) || e.key == "Home" || e.key == "End"){
      console.log(e.key);
    } else {
      e.preventDefault();
    }
  }

}

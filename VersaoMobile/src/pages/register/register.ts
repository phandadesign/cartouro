import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
//import auth-authentication
import {AngularFireAuth} from 'angularfire2/auth'
import { LoadingController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {


  @ViewChild('username') user;
  @ViewChild('password') password;



  constructor(private alertCtrl:AlertController,private fire: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController, public toastCtrl: ToastController ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  alert(message:string){
    this.alertCtrl.create({
      title:'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Usuário Cadastrado com Sucesso!',
      duration: 2000
    });
    toast.present();
  }

  presentLoading(message:string) {
    let loader = this.loadingCtrl.create({
      content: message,
      duration: 3000
    });
    loader.present();
  }

  registerUser(){
    let error = "";
    if(this.user.value == "" || this.user.value == undefined){
      error += "Preencha o campo E-mail\n";
    }
    if(this.password.value == "" || this.password.value == undefined){
      error += "Preencha o campo senha";
    }

    if(error == ""){
      //regiser new user
     this.fire.auth.createUserWithEmailAndPassword(this.user.value,this.password.value)
     .then(data =>{
         console.log('got data',data);
         this. presentLoading('Carregando...');
         this.presentToast();
         setTimeout(() => {
           this.navCtrl.push(LoginPage);
         }, 3000);
     })
     .catch(error =>{
        var errorCode = error.code;
        var errorMessage = error.message;
        if(errorMessage == "auth/invalid-email"){
         alert("Email Inválido");
        }else if(errorMessage == "The email address is badly formatted."){

         alert("Insira um Email válido");

        }else if(errorMessage == "Password should be at least 6 characters"){

         alert("A senha precisa de no mínimo 6 caracteres");
       } else if(errorMessage == "The email address is already in use by another account."){
         alert("E-mail já esta em uso.\nEscolha outro E-mail para registrar");
       } else {
         console.log(errorMessage);
       }
     });
    } else {
      alert(error);
    }

  }

}

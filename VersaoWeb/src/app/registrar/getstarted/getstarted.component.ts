import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../_services/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'getstarted',
  templateUrl: './getstarted.component.html',
  styleUrls: ['./getstarted.component.scss']
})
export class GetstartedComponent implements OnInit {
  public erro: any = {};
  public f: FormGroup;
  loading = false;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.f = this.formBuilder.group({
      email: ['', Validators.email],
      senha: ['', Validators.minLength(6)]
    });
  }

  limpaErros() {
    this.erro = {};
  }

  loginGoogle() {
    this.authService.loginGoogle();
  }

  signupWithGoogle(){
    
  }

  next() {
    this.erro = {};

    this.limpaErros();

    if (this.f.value['email'] == '') {
      this.erro = { email: 'Informe um email correto' };
    }
    if (this.f.value['senha'] < 6) {
      this.erro = { senha: 'A senha deve conter mais de 6 digitos' };
    }
    if (this.f.valid) {
      this.authService.registrarEmpresa(this.f.value['email'], this.f.value['senha'])
        .then((val) => {
          this.erro = null;
          this.authService.criarDatabaseEmpresa(val.user.uid, val.user.providerData[0])
            .then((result) => {
              window.localStorage.setItem('currentUser', JSON.stringify(val.user))
              this.router.navigate(['registrar/companhia']);
            });
        })
        .catch((erro) => {
          this.erro = erro;
          switch (erro) {
            case 'auth/email-already-in-use':
              erro.mensagem = 'Email já está cadastrado no sistema!';
              break;

            default:
              break;
          }
        });
      this.loading = false;
    }
  }

}
// 10492269
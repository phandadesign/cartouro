import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EscolherCartorioPage } from './escolher-cartorio';

@NgModule({
  declarations: [
    EscolherCartorioPage,
  ],
  imports: [
    IonicPageModule.forChild(EscolherCartorioPage),
  ],
})
export class EscolherCartorioPageModule {}

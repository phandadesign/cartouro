import { AngularMaterialModule } from './angular-material/angular-material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

/* Layouts */
import { FullLayoutComponent } from './_layouts/full-layout/full-layout.component';
import { SimpleLayoutComponent } from './_layouts/simple-layout/simple-layout.component';
import { Erro404Component } from './erro404/erro404.component';

import { LandingPageComponent } from './landing-page/landing-page.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './_layouts/navbar/navbar.component';

import { FlexLayoutModule } from "@angular/flex-layout";
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { HeaderDesktopComponent } from './_layouts/header-desktop/header-desktop.component';
import { ContainerComponent } from './_layouts/container/container.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

export const fbConfig = {
  apiKey: "AIzaSyBVYgNcTwZUwxA8NnRPxY7a2T92IOLDSBk",
  authDomain: "mastertickets-2018.firebaseapp.com",
  databaseURL: "https://mastertickets-2018.firebaseio.com",
  projectId: "mastertickets-2018",
  storageBucket: "",
  messagingSenderId: "596850746328"
};

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    Erro404Component,
    LandingPageComponent,
    NavbarComponent,
    FullLayoutComponent,
    HeaderDesktopComponent,
    ContainerComponent
  ],
  imports: [
    AppRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    AngularFireDatabaseModule,
    NgbModule.forRoot(),
    AngularFireModule.initializeApp(fbConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

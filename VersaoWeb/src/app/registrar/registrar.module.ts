import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrarRoutingModule } from './registrar-routing.module';
import { SharedModule } from '../shared/shared.module';
import { RegistrarComponent } from './registrar.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { CompanyComponent } from './company/company.component';
import { AboutyourselfComponent } from './aboutyourself/aboutyourself.component';
import { SucessoComponent } from './sucesso/sucesso.component';

@NgModule({
  imports: [
    CommonModule,
    RegistrarRoutingModule,
    SharedModule
  ],
  declarations: [
    RegistrarComponent,
    GetstartedComponent,
    CompanyComponent,
    AboutyourselfComponent,
    SucessoComponent
  ]
})
export class RegistrarModule { }

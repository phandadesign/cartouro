import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UsuarioService } from '../../_services/usuario.service';

@Component({
  selector: 'ctrheader-desktop',
  templateUrl: './header-desktop.component.html',
  styleUrls: ['./header-desktop.component.scss']
})
export class HeaderDesktopComponent implements OnInit {
  public loading = false;
  public nome_usuario = localStorage.getItem("nome_usuario");
  public status;

  user = JSON.parse(localStorage.getItem("info_user"));
  uid = JSON.parse(localStorage.getItem('current_user')).uid;
  caminho_db = localStorage.getItem("caminho_db");

  constructor(private router: Router, private userService: UsuarioService) {
    this.status = this.user.status;
  }

  ngOnInit() {
  }



  setStatus(status: any){
    this.user.status = status;

    this.userService.atualizarUsuario(this.user, this.uid).then(result => {
      this.userService.atualizarUsuarioDaEmpresa(this.caminho_db ,this.user, this.uid).then(r => {
        this.status = status;
        localStorage.setItem("info_user", JSON.stringify(this.user));
      }).catch(e => {
        alert(e);
      });
    }).catch(e => {
      alert(e);
    });
  }

  infoUsuario(){
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.router.navigate(['agent/configuracoes/']);
    }, 1500);
  }

  logoff(){
    localStorage.clear();
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
      this.router.navigate(['login/']);
    }, 3000);
  }

}

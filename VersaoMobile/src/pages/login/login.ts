import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
//import auth-authentication
import {AngularFireAuth} from 'angularfire2/auth'
import {RegisterPage} from '../../pages/register/register';
import { EscolherCartorioPage } from '../escolher-cartorio/escolher-cartorio';
import { ToastController } from 'ionic-angular';
import { LoadingController } from 'ionic-angular';
/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {


  @ViewChild('username') user;
  @ViewChild('password') password;

  constructor(public loadingCtrl: LoadingController,public toastCtrl: ToastController,private alertCtrl: AlertController,private fire: AngularFireAuth,public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  goRegister(){
    this.navCtrl.push(RegisterPage);
  }

  alert(message:string){
    this.alertCtrl.create({
      title:'Info!',
      subTitle: message,
      buttons: ['OK']
    }).present();
  }

  presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Login efetuado com sucesso',
      duration: 2000
    });
    toast.present();
  }
  presentLoading(message:string) {
    let loader = this.loadingCtrl.create({
      content: message,
      duration: 3000
    });
    loader.present();
  }



  signInUser(){
    let error = "";
    if(this.user.value == "" || this.user.value == undefined){
      error += "Preencha o campo Email\n";
    }
    if(this.password.value == "" || this.password.value == undefined){
      error += "Preencha o campo Senha";
    }

    if(error == ""){
      this.fire.auth.signInWithEmailAndPassword(this.user.value,this.password.value)
      .then(data =>{
          console.log('got some data', this.fire.auth.currentUser);
          // logged success
          this. presentLoading('Carregando...');
          this.presentToast();
          this.navCtrl.push(EscolherCartorioPage)
      })
      .catch(error => {
        console.log('got an error',error)
        var errorCode = error.code;
        var errorMessage = error.message;

        if(errorMessage == "auth/invalid-email"){
          alert("Email ou senha Incorretos");
        }else if(errorMessage == "auth/wrong-password"){
          alert("Email ou senha Incorretos");
        }else if(errorMessage == "auth/user-not-found"){
          alert("Usuário Não Cadastrado");
        }else if(errorMessage == "The email address is badly formatted."){
          alert("Insira um Email válido");
        }else{
          alert("Usuário ou senha Inválido");
        }
        console.log(errorMessage);
      });
    } else {
      alert(error);
    }


  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';
import { VisualizacoesComponent } from './visualizacoes/visualizacoes.component';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { AtenderSenhaComponent } from './atender-senha/atender-senha.component';
import { CriarUsuarioComponent } from './criar-usuario/criar-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios.component';

const routes: Routes = [
  { path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  { path: 'dashboard', component: DashboardComponent },
  { path: 'relatorios', component: RelatoriosComponent },
  { path: 'visualizacoes', component: VisualizacoesComponent },
  { path: 'configuracoes', component: ConfiguracoesComponent },
  { path: 'atendimentos', component: AtenderSenhaComponent },
  { path: 'atender-senha', component: AtenderSenhaComponent },
  { path: 'criar-usuario', component: CriarUsuarioComponent },
  { path: 'usuarios', component: UsuariosComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgentRoutingModule { }

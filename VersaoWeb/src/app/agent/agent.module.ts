import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AgentRoutingModule } from './agent-routing.module';
import { ConfiguracoesComponent } from './configuracoes/configuracoes.component';
import { VisualizacoesComponent } from './visualizacoes/visualizacoes.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SharedModule } from '../shared/shared.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AtenderSenhaComponent } from './atender-senha/atender-senha.component';
import { CriarUsuarioComponent } from './criar-usuario/criar-usuario.component';
import { UsuariosComponent } from './usuarios/usuarios.component';

@NgModule({
  imports: [
    CommonModule,
    AgentRoutingModule,
    SharedModule,
    NgxChartsModule,
    FormsModule
  ],
declarations: [ConfiguracoesComponent, VisualizacoesComponent, RelatoriosComponent, DashboardComponent, AtenderSenhaComponent, CriarUsuarioComponent, UsuariosComponent]
})
export class AgentModule { }

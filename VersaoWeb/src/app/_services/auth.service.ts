import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';

import { AngularFireDatabase } from 'angularfire2/database'
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import { Empresa } from '../_models/empresa';
import { Usuario } from '../_models/usuario';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public usuarios: Observable<any[]>;
  private currentEmpresa: Empresa;
  private currentUser: Usuario;

  constructor(
    private afAuth: AngularFireAuth,
    public db: AngularFireDatabase
  ) { }

  getCurrentEmpresa(): Observable<any> {
    let current_user = this.getCurrentUser();
    return this.db.object(`/contas/${current_user.key}/key_empresa_associada/`).valueChanges();
  }

  getCurrentUser() {
    return JSON.parse(window.localStorage.getItem('current_user'));
  }

  login(e, s) {
    return this.afAuth.auth.signInWithEmailAndPassword(e, s)
      .then((result) => {
        this.saveCurrentUser(JSON.stringify(result.user));
      });
  }

  registrarEmpresa(e, s): Promise<any> {
    return this.afAuth.auth.createUserWithEmailAndPassword(e, s);
  }

  registrarUsuario(e, s): Promise<any> {
    return this.afAuth.auth.createUserWithEmailAndPassword(e, s);
  }

  criarDatabaseEmpresa(codigoEmpresa, dataInicial): Promise<any> {
    return this.db.list(`/empresas/`).set(codigoEmpresa, dataInicial);
  }

  saveCurrentUser(user) { // SALVAR DADOS DO USUARIO LOGADO
    if(localStorage.getItem("current_user") == undefined){
      localStorage.setItem('current_user', user);
    }
  }

  deletarUsuario() {
    let user = this.afAuth.auth.currentUser;
    user.delete().then(success => { console.log('Sucesso ao deletar email!', success) }).catch(erro => { console.log('Erro ao deletar email!', erro) });
  }

  pegarBaseDeDados(email) {
    this.usuarios = this.db.list("/usuarios/", ref => ref.orderByChild("email").equalTo(email)).snapshotChanges().pipe(map(arr => {
      return arr.map(snap => Object.assign(snap.payload.val(), { key: snap.key }))
    })
    );
    localStorage.setItem("usuario", JSON.stringify(this.afAuth.auth.currentUser));

    this.usuarios.forEach(val => {
      localStorage.setItem("caminho_db", val[0].basededados);
      localStorage.setItem("nome_usuario", val[0].nome);
      localStorage.setItem("info_user", JSON.stringify(val[0]));
    });
  }

  loginGoogle() {
    // this.afAuth.auth.signInWithRedirect();
  }

  isAuthorize(): boolean {
    const user = JSON.parse(localStorage.getItem('current_user'));
    if (user !== null) {
      return true;
    } else {
      return false;
    }
  }

  atualizarDadosEmpresa(params: any) {
    let codigo_empresa = params.codigo_empresa;
    return this.db.list(`/empresas/`).update(codigo_empresa, params.data);
  }

  //Cria a base de dados da empresa.
  criarBaseDeDadosEmpresa(params: any) {
    let codigo_empresa = params.codigo_empresa;
    let nome_empresa = this.afAuth.auth.currentUser.displayName;
    let dadosBasicos = { "NomeEmpresa": nome_empresa }
    return this.db.list(`/bancodedadosgeral/`).set(codigo_empresa, dadosBasicos);
  }

  criarUsuarioGenerico(params: any): any {
    let codigo_empresa = params.codigo_empresa;
    return this.db.list(`/usuarios/`).set(codigo_empresa, params.data.contas[`${codigo_empresa}`]);
  }


  criarUsuarioDoCartorio(params: any): any {

    return this.db.list(`/usuarios/`).push(params.data);

  }

  criarUsuarioPelaEmpresa(params: any, key: string): any {
    console.log(key);
    return this.db.list(`/usuarios/`).set(key, params);

  }

  adicionarUsuarioAoCartorio(params: any, empresa: any, key: any): any {
    console.log(empresa);
    return this.db.list(`/empresas/${empresa}/contas/`).set(key, params); //this.db.list(`/usuarios/`).set(key, params);
  }
/*
  getKey(val: any){
      return new Promise(resolve => {
        localStorage.setItem("novo_usuario", val.key);
        resolve(val);
      });
  }
*/
  cadastroGeralCartorio(params: any): any{
    return this.db.list("/todasempresas/").set(params.caminho_db, params);
  }

  deslogaUsuario() {
    window.localStorage.clear();
    window.sessionStorage.clear();
  }

}

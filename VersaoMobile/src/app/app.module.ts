import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import AngularFireModule (install and config ) install angularfire2 --> npm install firebase angularfire2 --save
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';

//import AngularFireDatabaseModule
import {AngularFireDatabaseModule} from 'angularfire2/database'

//import class register,loginPage
import{RegisterPage} from '../pages/register/register';
import{LoginPage} from '../pages/login/login'

import { EscolherCartorioPage } from '../pages/escolher-cartorio/escolher-cartorio';

/*
//Base de dados do João

const firebaseAuth = {
  apiKey: "AIzaSyDOPTzP3cQXyy2QsDJ4z1FiUnZbDvTmRY4",
  authDomain: "cartorio-c86b4.firebaseapp.com",
  databaseURL: "https://cartorio-c86b4.firebaseio.com",
  projectId: "cartorio-c86b4",
  storageBucket: "cartorio-c86b4.appspot.com",
  messagingSenderId: "204016683419"
};
*/

//Base de dados Compartilhada
const firebaseAuth = {
  apiKey: "AIzaSyBVYgNcTwZUwxA8NnRPxY7a2T92IOLDSBk",
  authDomain: "mastertickets-2018.firebaseapp.com",
  databaseURL: "https://mastertickets-2018.firebaseio.com",
  projectId: "mastertickets-2018",
  storageBucket: "mastertickets-2018.appspot.com",
  messagingSenderId: "596850746328"
};



@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    RegisterPage,
    LoginPage,
    EscolherCartorioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    //import firebase (install and config )
    AngularFireModule.initializeApp(firebaseAuth),
    AngularFireAuthModule,
    AngularFireDatabaseModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    RegisterPage,
    LoginPage,
    EscolherCartorioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
    
  ]
})
export class AppModule {}

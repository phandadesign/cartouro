import { LandingPageComponent } from './landing-page/landing-page.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/* Layouts */
import { FullLayoutComponent } from './_layouts/full-layout/full-layout.component';
import { SimpleLayoutComponent } from './_layouts/simple-layout/simple-layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { Erro404Component } from './erro404/erro404.component';


/* Guards */
import { AuthGuard } from './_guards/auth.guard';
import { AdminGuard } from './_guards/admin.guard';

const routes: Routes = [
  // { path: '', component: LandingPageComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'registrar', component: SimpleLayoutComponent, children: [
      { path: '', loadChildren: './registrar/registrar.module#RegistrarModule' }
    ]
  },
  {
    path: 'login', component: SimpleLayoutComponent, children: [
      { path: '', loadChildren: './login/login.module#LoginModule' }
    ]
  },
  {
    path: 'agent', component: FullLayoutComponent, children: [
      { path: '', loadChildren: './agent/agent.module#AgentModule', canActivate: [AuthGuard] }
    ]
  },
  {
    path: 'admin', component: FullLayoutComponent, children: [
      { path: '', loadChildren: './admin/admin.module#AdminModule', canActivate: [AuthGuard, AdminGuard] }
    ]
  },
  { path: 'erro404', component: Erro404Component },
  {
    path: '**', redirectTo: 'erro404', pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

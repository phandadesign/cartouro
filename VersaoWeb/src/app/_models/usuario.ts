import { Conta } from '../_interfaces/conta';

export class Usuario implements Conta {
  key: string;
  senha: string;
  nome: string;
  email: string;
  permissao: string;
}
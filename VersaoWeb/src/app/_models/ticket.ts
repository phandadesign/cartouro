class Ticket {
  public contador: number = 0;
  constructor(){
    this.cronometro();
  }
  /**
   * cronometro
   */
  public cronometro() {
    setTimeout(() => {
      this.contador++;
    }, 100);
  }
}